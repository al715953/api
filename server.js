var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

app.use(function(req, res, next) {
 res.header("Access-Control-Allow-Origin", "*");
 res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
 next();
});


var requestjson= require('request-json');
var path = require('path');
var urlmovimientosMlab = 'https://api.mlab.com/api/1/databases/bdbanca2hlr/collections/movimientos?apiKey=ZCfAhJuS_-bVqga6dGGP7TmKaRVjxH32';
var clienteMLab = requestjson.createClient(urlmovimientosMlab);

var bodyParser = require('body-parser');
app.use(bodyParser.json());

/*var data=
{
    "idcliente": 9999,
    "nombre": "Paco",
    "apellido": "Modarse"
};
*/

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

app.get("/", function(req,res)
  {
    res.sendFile(path.join(__dirname, "index.html"));
  }
)


app.get("/movimientos", function(req,res)
  {
    clienteMLab.get('',function(err,resM,body)
    {
      if(err)
      {
        console.log(body);
      }
      else {
        res.send(body);
      }
    }
  )}
)
//-----------------------------
/*app.post("/movimientos",function(req,res)
  {
    clienteMLab.post('',data,function(err,resM,body)
    {
      if(err)
      {
        console.log(body);
      }
      else {
        res.send(body);
        console.log(body);
      }
    }
  )}
)*/

//Usando el parse de body para obtener una respuesta
app.post("/movimientos",function(req,res)
  {
    clienteMLab.post('',req.body,function(err,resM,body)
    {
      if(err)
      {
        console.log(body);
      }
      else {
        res.send(body);
      }
    }
  )}
)




app.post("/", function(req,res)
{
  res.send("Hemos recibido su petición");
}

)
app.put("/", function(req,res)
{
  res.send("Hemos recibido su put");
}
)
app.delete("/", function(req,res)
{
  res.send("Hemos recibido su delete");
}
)

app.get("/clientes/:idcliente", function(req,res)
{
  res.send("Aquí tiene el cliente número: "+req.params.idcliente);
}
)
